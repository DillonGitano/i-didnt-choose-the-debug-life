$('.open_controls').click(function(e) {
  e.preventDefault();
  
  $(this).siblings('.controls').fadeToggle(300);
});

$("#rose").hide().one("load",function(){
    $(this).fadeIn(5000);
}).each(function(){
    if(this.complete) $(this).trigger("load");
});
  
var $targets = $('.target');
    $('.link').click(function () {
        var $target = $($(this).data('target')).slideToggle();
        $targets.not($target).hide()
        $('#mainphoto').hide();
        $('.self').hide();
    });
    
 $("#hideSister").on("click", function(){
    $("#Valerie").hide();
  });
  $("#showSister").on("click", function(){
    $("#Valerie").show();
  });
  
  $("#hideFox").on("click", function(){
    $("#spirit").hide();
  });
  $("#showFox").on("click", function(){
    $("#spirit").show();
  });
  